import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Layout'),
        debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Container(
          constraints: BoxConstraints.expand(),
          color: Colors.lightBlueAccent[100],
          child: SingleChildScrollView(
          child: Column(
            children: [
              Icon(
                //Icons.insert_photo,
                Icons.account_circle_rounded,
                size: 200,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    "Row Child 1",
                    style: TextStyle(fontSize: 18),
                  ),
                  Text(
                    "Row Child 2",
                    style: TextStyle(fontSize: 18),
                  ),
                  Text(
                    "Row Child 3",
                    style: TextStyle(fontSize: 18),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(30.0),
                child: Text(
                  "This is column.",
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                ),
              ),


            ],
          ),
          ),
        ));
  }


}

